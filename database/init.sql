DROP DATABASE data;
ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'Aa123456';
FLUSH PRIVILEGES;

CREATE DATABASE data;

USE data;
CREATE TABLE A(
    Cas TIMESTAMP,
    Teplota FLOAT(4,2),
    IDZarizeni INT
);
CREATE TABLE B(
    Cas TIMESTAMP,
    IDZarizeni INT
);
CREATE TABLE C(
    Cas TIMESTAMP,
    ID INT,
    IDZarizeni VARCHAR(10),
    Zaznam VARCHAR(255)
);